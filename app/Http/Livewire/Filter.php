<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;

class Filter extends Component
{
    use WithPagination;

    public $searchTerm;
    public $currentPage = 1;
    public $allPosts, $title, $description, $postId, $updatePost = false, $addPost = false;

    protected $rules = [
        'title' => 'required',
        'description' => 'required'
    ];

    public function resetFields()
    {
        $this->title = '';
        $this->description = '';
    }

    public function render()
    {
        $query = '%' . $this->searchTerm . '%';
        $this->allPosts = Post::select('id', 'title', 'description')->get();
        return view('livewire.filter', [
            'posts' => Post::where(function ($sub_query) {
                $sub_query->where('title', 'like', '%' . $this->searchTerm . '%')
                    ->orWhere('description', 'like', '%' . $this->searchTerm . '%');
            })->paginate(5), 'allposts' => $this->allPosts
        ]);
    }

    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function editPost($id)
    {
        try {
            $post = Post::findOrFail($id);
            if (!$post) {
                session()->flash('error', 'Post not found');
            } else {
                $this->title = $post->title;
                $this->description = $post->description;
                $this->postId = $post->id;
                $this->updatePost = true;
            }
        } catch (\Exception $ex) {
            session()->flash('error', 'Something goes wrong!!');
        }
    }

    public function updatePost()
    {
        $this->validate();
        try {
            Post::whereId($this->postId)->update([
                'title' => $this->title,
                'description' => $this->description
            ]);
            session()->flash('success', 'Post Updated Successfully!!');
            $this->resetFields();
            $this->updatePost = false;
        } catch (\Exception $ex) {
            session()->flash('success', 'Something goes wrong!!');
        }
    }

    public function cancelPost()
    {
        $this->updatePost = false;
        $this->resetFields();
    }

    public function deletePost($id)
    {
        try {
            Post::find($id)->delete();
            session()->flash('success', "Post Deleted Successfully!!");
        } catch (\Exception $e) {
            session()->flash('error', "Something goes wrong!!");
        }
    }
}
