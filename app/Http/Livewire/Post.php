<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Post as Posts;
use Livewire\WithPagination;

class Post extends Component
{
    use WithPagination;

    public $posts, $title, $description, $postId, $addPost = false,$updatePost = false;

    protected $rules = [
        'title' => 'required',
        'description' => 'required'
    ];

    public function resetFields()
    {
        $this->title = '';
        $this->description = '';
    }

    public function render()
    {

        $this->posts = Posts::select('id', 'title', 'description')->get();

        return view('livewire.post');
    }

    public function addPost()
    {
        $this->resetFields();
        $this->addPost = true;
    }

    public function storePost()
    {
        $this->validate();
        try {
            Posts::create([
                'title' => $this->title,
                'description' => $this->description
            ]);
            session()->flash('success', 'Post Created Successfully!!');
            $this->resetFields();
            $this->addPost = false;
        } catch (\Exception $ex) {
            session()->flash('error', 'Something goes wrong!!');
        }
    }

    public function cancelPost()
    {
        $this->addPost = false;
        $this->resetFields();
    }

}
