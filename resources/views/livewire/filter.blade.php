<div>
    @if ($updatePost)
        @include('livewire.update')
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Search" wire:model="searchTerm" />
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2">Title</th>
                                <th class="px-4 py-2">Description</th>
                                <th class="px-4 py-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($allPosts) > 0)
                                @foreach ($posts as $post)
                                    <tr>
                                        <td class="border px-4 py-2">{{ $post->id }}</td>
                                        <td class="border px-4 py-2">{{ $post->title }}</td>
                                        <td class="border px-4 py-2">{{ $post->description }}</td>
                                        <td>
                                            <button wire:click="editPost({{ $post->id }})"
                                                class="btn btn-primary btn-sm">Edit</button>
                                            <button wire:click="deletePost({{ $post->id }})"
                                                class="btn btn-danger btn-sm">Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" align="center">
                                        No Posts Found.
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $posts?->links('livewire.livewire-pagination') }}
            </div>
        </div>
    </div>
</div>
